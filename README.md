# Seven Acres Ferments Ecommerce App - Rails Backend

Master branch automatically deployed to Heroku using webhook.

Hosted at <https://square-api.sevenacresfarm.ca>.

Using Rails/Heroku for quick development and deployment.


## Build Setup

``` bash
# install dependencies
$ bundle

# serve at localhost:3000
$ rails server
```

