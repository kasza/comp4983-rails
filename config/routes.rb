Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'hellos#index'
  get 'hello', to: 'hellos#index'
  get 'hellos', to: 'hellos#index'

  post 'charges/charge_card'
  resources :orders, only: :create
  resources :products, only: %i[index show]
  resources :categories, only: :index
end
