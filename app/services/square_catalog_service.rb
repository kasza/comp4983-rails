# /app/services/square_catalog_service.rb
class SquareCatalogService
  attr_reader :catalog, :catalog_items, :catalog_categories, :categories, :products, :product

  def initialize
    @catalog = nil
    @catalog_categories = []
    @catalog_items = []
    @categories = []
    @products = []
    @catalog_api = SquareConnect::CatalogApi.new
  end

  def retrieve_items
    @catalog_items = retrieve_catalog('item')
    @catalog_items
  end

  def retrieve_catalog_categories
    @catalog_categories = retrieve_catalog('category')
    @catalog_categories
  end

  def retrieve_item(id)
    opts = {
      include_related_objects: true
    }

    cache_key = ['item', id]
    @item = Rails.cache.fetch(cache_key, expires_in: 24.hours) do
      puts "pulling item #{id} from square"
      @catalog_api.retrieve_catalog_object(id, opts)
    end

    @item
  rescue SquareConnect::ApiError => e
    puts "Exception when calling CatalogApi->retrieve_catalog_object: #{e}"
  end

  def generate_categories
    retrieve_catalog_categories if @catalog_categories == []

    @catalog_categories.objects.each do |cat|
      cat = Category.new(cat)
      @categories << cat
    end

    @categories
  end

  def generate_products
    retrieve_items if @catalog_items == []

    @catalog_items.objects.each do |cat_item|
      item = retrieve_item(cat_item.id)
      @products << generate_product(item)
    end

    @products
  end

  def generate_product(item)
    @product = Product.new(item)
    @product
  end

  protected

  def retrieve_catalog(types)
    opts = {
      cursor: '',
      types: types.upcase
    }

    cache_key = ['square_catalog', opts]
    Rails.cache.fetch(cache_key, expires_in: 24.hours) do
      puts "pulling #{types} from square"
      @catalog_api.list_catalog(opts)
    end
  rescue SquareConnect::ApiError => e
    puts "Exception when calling CatalogApi->list_catalog: #{e}"
  end
end
