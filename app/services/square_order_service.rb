# /app/services/square_order_service.rb
class SquareOrderService
  attr_reader :order_line_items

  def initialize
    @order_line_items = []
    @order_request_line_items = []
    @total_money = SquareConnect::Money.new
    @total_money.amount = 0
    @total_money.currency = 'CAD'
    @order = nil
    @order_request = nil
    @checkout = nil
    @idempotency_key = generate_idempotency_key
    @checkout_api = SquareConnect::CheckoutApi.new
    @location_id = Rails.application.credentials.square[:location_id]
  end

  def add_items(items)
    items.each do |item|
      add_order_line_item(item)
      add_order_request_line_item(item)
      @total_money.amount += (item['price'] * item['quantity'])
    end
  end

  def setup_checkout
    create_order
    create_order_request
    create_checkout
  end

  def url
    return nil if @line_items == []

    setup_checkout if @checkout.nil?

    @checkout.checkout.checkout_page_url
  end

  protected

  def add_order_line_item(item)
    line_item = SquareConnect::OrderLineItem.new
    line_item.quantity = item['quantity'].to_s
    line_item.catalog_object_id = item['variant_id']
    @order_line_items << line_item
  end

  def add_order_request_line_item(item)
    line_item = SquareConnect::CreateOrderRequestLineItem.new
    line_item.quantity = item['quantity'].to_s
    line_item.catalog_object_id = item['variant_id']
    @order_request_line_items << line_item
  end

  def create_order
    @order = SquareConnect::Order.new
    @order.location_id = @location_id
    @order.line_items = @order_line_items
    @order.total_money = @total_money
  end

  def create_order_request
    @order_request = SquareConnect::CreateOrderRequest.new
    @order_request.order = @order
    @order_request.line_items = @order_request_line_items
  end

  def create_checkout
    body = SquareConnect::CreateCheckoutRequest.new
    body.ask_for_shipping_address = true
    body.idempotency_key = generate_idempotency_key
    body.order = @order_request
    body.redirect_url = 'https://shop.sevenacresfarm.ca/thank-you'

    begin
      @checkout = @checkout_api.create_checkout(@location_id, body)
    rescue SquareConnect::ApiError => e
      puts "Exception when calling CheckoutApi->create_checkout: #{e}"
    end
  end

  def generate_idempotency_key
    date = Time.zone.now
    str = date.to_s + rand(99).to_s
    Digest::MD5.hexdigest(str)
  end
end
