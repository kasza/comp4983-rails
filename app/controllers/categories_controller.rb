# /app/controllers/categories_controller.rb
class CategoriesController < ApplicationController
  def index
    catalog = SquareCatalogService.new
    catalog.retrieve_categories
    json_response(catalog.categories)
  end
end
