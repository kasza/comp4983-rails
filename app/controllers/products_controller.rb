# /app/controllers/products_controller.rb
class ProductsController < ApplicationController
  # GET /products
  def index
    catalog = SquareCatalogService.new
    products = catalog.generate_products
    json_response(products)
  end

  # GET /products/:id
  def show
    catalog = SquareCatalogService.new
    item = catalog.retrieve_item(params[:id])
    product = catalog.generate_product(item)
    json_response(product)
  end

  protected

  def product_params
    params.permit(:id)
  end
end
