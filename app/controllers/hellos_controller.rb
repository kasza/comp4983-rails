# /app/controllers/hellos_controller.rb
class HellosController < ApplicationController
  # GET /hellos
  def index
    json_response(hello: 'world')
  end
end
