# /app/controllers/orders_controller.rb
class OrdersController < ApplicationController
  # POST /orders
  def create
    order = SquareOrderService.new
    order.add_items(params[:items])
    json_response({ url: order.url }, :ok)
  end

  protected

  def product_params
    params.permit(:items)
  end
end
