# /app/controllers/concerns/array_hash.rb
module ArrayHash
  def to_hash(array, key = :id)
    Hash[array.map { |item| [item.send(key), item] }]
  end
end
