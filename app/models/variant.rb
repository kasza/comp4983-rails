# /app/models/variant.rb
class Variant
  include ActiveModel::Model

  attr_accessor :id, :price, :size

  def initialize(square_variant)
    @id = square_variant.id
    @price = square_variant.item_variation_data.price_money.amount
    @size = square_variant.item_variation_data.name
  end
end
