# /app/models/category.rb
class Category
  include ActiveModel::Model

  attr_accessor :id, :name

  def initialize(item)
    err_msg = "Invalid type; catalog must be 'SquareConnect::CatalogObject'"
    raise TypeError, err_msg unless item.class.name == 'SquareConnect::CatalogObject'

    @id = item.id
    @name = item.category_data.name
  end
end
