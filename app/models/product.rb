# /app/models/product.rb
class Product
  include ActiveModel::Model

  attr_reader :category_id, :description, :id, :name, :variants
  attr_accessor :category

  def initialize(item)
    err_msg = "Invalid type; product must be 'SquareConnect::RetrieveCatalogObjectResponse'"
    raise TypeError, err_msg unless item.class.name == 'SquareConnect::RetrieveCatalogObjectResponse'

    @id = item.object.id
    @variants = []
    populate_data(item.object.item_data)
    populate_related_objects(item.related_objects) if item.instance_variable_defined?(:@related_objects)
  end

  protected

  def populate_data(item_data)
    @name = item_data.name
    @description = item_data.description
    load_variants(item_data.variations)
  end

  def populate_related_objects(objects)
    objects.each do |obj|
      send("load_#{obj.type.downcase}", obj)
    end
  end

  def load_variants(variants)
    variants.each do |v|
      variant = Variant.new(v)
      @variants << variant
    end
  end

  def load_category(data)
    @category_id = data.id
    @category = data.category_data.name
  end

  def load_image(data)
    @image_url = data.image_data.url
  end
end
